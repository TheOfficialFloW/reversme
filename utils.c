/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "game.h"
#include "utils.h"

uint32_t hold_buttons, released_buttons, current_buttons, old_buttons;

Position released_touch = INVALID_POSITION, current_touch = INVALID_POSITION, old_touch = INVALID_POSITION;

void ReadPad()
{
	SceTouchData touch;
	memset(&touch, 0, sizeof(SceTouchData));
	sceTouchPeek(0, &touch, 1);

	old_touch = current_touch;

	if (touch.reportNum > 0) {
		current_touch = (Position){(int)((float)touch.report[0].x / 2.0f), (int)((float)touch.report[0].y / 2.0f)};
	} else {
		current_touch = INVALID_POSITION;
	}

	if (old_touch.x != -1 && old_touch.y != -1 && current_touch.x == -1 && current_touch.y == -1) {
		released_touch = old_touch;
	} else {
		released_touch = INVALID_POSITION;
	}

	static int n = 0;

	SceCtrlData pad;
	memset(&pad, 0, sizeof(SceCtrlData));
	sceCtrlPeekBufferPositive(0, &pad, 1);

	if (pad.ly < 0x40)
		pad.buttons |= PSP2_CTRL_LEFT_ANALOG_UP;
	else if (pad.ly > 0xC0)
		pad.buttons |= PSP2_CTRL_LEFT_ANALOG_DOWN;

	if (pad.lx < 0x40)
		pad.buttons |= PSP2_CTRL_LEFT_ANALOG_LEFT;
	else if (pad.lx > 0xC0)
		pad.buttons |= PSP2_CTRL_LEFT_ANALOG_RIGHT;

	if (pad.ry < 0x40)
		pad.buttons |= PSP2_CTRL_RIGHT_ANALOG_UP;
	else if (pad.ry > 0xC0)
		pad.buttons |= PSP2_CTRL_RIGHT_ANALOG_DOWN;

	if (pad.rx < 0x40)
		pad.buttons |= PSP2_CTRL_RIGHT_ANALOG_LEFT;
	else if (pad.rx > 0xC0)
		pad.buttons |= PSP2_CTRL_RIGHT_ANALOG_RIGHT;

	current_buttons = pad.buttons;
	released_buttons = ~current_buttons & old_buttons;
	hold_buttons = current_buttons & ~old_buttons;

	if (old_buttons == current_buttons) {
		n++;
		if (n >= 14) {
			hold_buttons = current_buttons;
			n = 10;
		}
	} else {
		n = 0;
		old_buttons = current_buttons;
	}
}

int random(int min, int max) {
   return min + rand() / (RAND_MAX / (max - min + 1) + 1);
}

void randomWait(int min, int max) {
    sceKernelDelayThread(random(min, max));
}

Position alignCenter(Position pos0, Position pos1, int width, int height) {
	return (Position){(pos0.x + pos1.x - width) / 2, (pos0.y + pos1.y - height) / 2};
}

void drawRectangle(Position pos, int width, int height, uint32_t color) {
	vita2d_draw_rectangle(pos.x, pos.y, width, height, color);
}

void drawBorder(Position pos, int width, int height, int tightness, uint32_t color) {
	vita2d_draw_rectangle(pos.x, pos.y, width, tightness, color);
	vita2d_draw_rectangle(pos.x, pos.y + height - tightness, width, tightness, color);
	vita2d_draw_rectangle(pos.x, pos.y, tightness, height, color);
	vita2d_draw_rectangle(pos.x + width - tightness, pos.y, tightness, height, color);
}