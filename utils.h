/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __UTILS_H__
#define __UTILS_H__

#define PSP2_CTRL_RIGHT_ANALOG_UP		0x0020000
#define PSP2_CTRL_RIGHT_ANALOG_RIGHT	0x0040000
#define PSP2_CTRL_RIGHT_ANALOG_DOWN		0x0080000
#define PSP2_CTRL_RIGHT_ANALOG_LEFT		0x0100000

#define PSP2_CTRL_LEFT_ANALOG_UP		0x0200000
#define PSP2_CTRL_LEFT_ANALOG_RIGHT		0x0400000
#define PSP2_CTRL_LEFT_ANALOG_DOWN		0x0800000
#define PSP2_CTRL_LEFT_ANALOG_LEFT		0x1000000

typedef struct {
	int x;
	int y;
} Position;

#define INVALID_POSITION (Position){-1, -1}

extern uint32_t hold_buttons, released_buttons, current_buttons, old_buttons;

extern Position released_touch, current_touch_pos, old_touch_pos;

void ReadPad();
int random(int min, int max);
void randomWait(int min, int max);
Position alignCenter(Position pos0, Position pos1, int width, int height);
void drawRectangle(Position pos, int width, int height, uint32_t color);
void drawBorder(Position pos, int width, int height, int tightness, uint32_t color);

#endif