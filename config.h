/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CONFIG_H__
#define __CONFIG_H__

#define CONFIG_PATH "cache0:/VitaDefilerClient/Documents/reversme_config.bin"

typedef struct {
	int player;
	int ai_level;
	int show_valid_moves;
} ReversMeConfig;

void loadConfig(ReversMeConfig *config);
void saveConfig(ReversMeConfig *config);

#endif