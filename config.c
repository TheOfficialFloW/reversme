/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "config.h"

void setDefaultConfig(ReversMeConfig *config) {
	config->player = PIECE_PURPLE;
	config->ai_level = AI_LEVEL_DEFAULT;
	config->show_valid_moves = 1;
}

void loadConfig(ReversMeConfig *config) {
	setDefaultConfig(config);

	SceUID fd = sceIoOpen(CONFIG_PATH, PSP2_O_RDONLY, 0);
	if (fd >= 0) {
		if (sceIoRead(fd, config, sizeof(ReversMeConfig)) != sizeof(ReversMeConfig)) {
			setDefaultConfig(config);
		}

		sceIoClose(fd);
	}
}

void saveConfig(ReversMeConfig *config) {
	SceUID fd = sceIoOpen(CONFIG_PATH, PSP2_O_WRONLY | PSP2_O_CREAT | PSP2_O_TRUNC, 0777);
	if (fd >= 0) {
		sceIoWrite(fd, config, sizeof(ReversMeConfig));
		sceIoClose(fd);
	}
}