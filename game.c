/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "game.h"
#include "utils.h"

int getPiece(Board *board, Position pos) {
	return board->board[pos.x][pos.y];
}

void setPiece(Board *board, Position pos, int piece) {
	board->board[pos.x][pos.y] = piece;
}

Position getPositionByPiece(Position pos) {
	return (Position){BOARD_X + pos.x * PIECE_TOTAL_SIZE, BOARD_Y + pos.y * PIECE_TOTAL_SIZE};
}

Position getPieceByPosition(Position pos) {
	return (Position){(int)floorf(((float)(pos.x - BOARD_X) / (float)PIECE_TOTAL_SIZE)), (int)floorf(((float)(pos.y - BOARD_Y) / (float)PIECE_TOTAL_SIZE))};
}

int isOnBoard(Position pos) {
	if (pos.x >= 0 && pos.x < BOARD_RANGE && pos.y >= 0 && pos.y < BOARD_RANGE)
		return 1;

	return 0;
}

int getOpponent(int piece) {
	switch (piece) {
		case PIECE_PURPLE:
			piece = PIECE_GREEN;
			break;
		
		case PIECE_GREEN:
			piece = PIECE_PURPLE;
			break;
	}

	return piece;
}

Position *getPiecesToReverse(Board *board, Position pos, int player, int check_valid_piece) {
	// Piece already occupied
	if (getPiece(board, pos) != PIECE_EMPTY)
		return NULL;

	int pieces_count = 0;
	static Position pieces[BOARD_LEN];

	if (!check_valid_piece)
		memset(pieces, -1, sizeof(pieces));

	// Direction vectors
	static int directions[8][2] = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};

	int i;
	for (i = 0; i < 8; i++) {
		Position position = pos;

		int connection_piece_exist = 0;
		int opponent_piece_count = 0;

		// Find connection piece
		while (1) {
			position.x += directions[i][0];
			position.y += directions[i][1];

			if (!isOnBoard(position))
				break;

			int piece = getPiece(board, position);
			if (piece == PIECE_EMPTY) {
				break;
			} else if (piece == player) {
				connection_piece_exist = 1;
				break;
			} else if (piece == getOpponent(player)) {
				opponent_piece_count++;
			}
		}

		// Go backwards
		if (connection_piece_exist) {
			// Minimum one row to reverse: valid move
			if (check_valid_piece && opponent_piece_count > 0)
				return pieces;
			
			// Append all pieces
			while (opponent_piece_count > 0) {
				position.x -= directions[i][0];
				position.y -= directions[i][1];
				pieces[pieces_count] = position;
				opponent_piece_count--;
				pieces_count++;
			}
		}
	}

	// Invalid move
	if (pieces_count == 0)
		return NULL;

	// Return list of coordinates
	return pieces;
}

void reversePieces(Board *board, Position pos, int player) {
	Position *pieces = getPiecesToReverse(board, pos, player, 0);
	if (pieces) {
		int i;
		for (i = 0; i < BOARD_LEN; i++) {
			if (isOnBoard(pieces[i]))
				setPiece(board, pieces[i], player);
		}

		setPiece(board, pos, player);
	}
}

Position getScore(Board *board) {
	Position score = (Position){0, 0};

	int x;
	for (x = 0; x < BOARD_RANGE; x++) {
		int y;
		for (y = 0; y < BOARD_RANGE; y++) {
			int piece = getPiece(board, (Position){x, y});
			if (piece == PIECE_PURPLE) {
				score.x++;
			} else if (piece == PIECE_GREEN) {
				score.y++;
			}
		}
	}

	return score;
}

int noValidMoves(Board *board, int player) {
	int x;
	for (x = 0; x < BOARD_RANGE; x++) {
		int y;
		for (y = 0; y < BOARD_RANGE; y++) {
			if (getPiecesToReverse(board, (Position){x, y}, player, 1))
				return 0;
		}
	}

	return 1;
}

int numValidMoves(Board *board, int player) {
	int count = 0;

	int x;
	for (x = 0; x < BOARD_RANGE; x++) {
		int y;
		for (y = 0; y < BOARD_RANGE; y++) {
			if (getPiecesToReverse(board, (Position){x, y}, player, 1))
				count++;
		}
	}

	return count;
}

int isGameOver(Board *board) {
	if (noValidMoves(board, PIECE_GREEN) && noValidMoves(board, PIECE_PURPLE))
		return 1;

	return 0;
}

void initBoard(Board *board) {
	int x;
	for (x = 0; x < BOARD_RANGE; x++) {
		int y;
		for (y = 0; y < BOARD_RANGE; y++) {
			setPiece(board, (Position){x, y}, PIECE_EMPTY);
		}
	}

	setPiece(board, (Position){3, 3}, PIECE_GREEN);
	setPiece(board, (Position){3, 4}, PIECE_PURPLE);
	setPiece(board, (Position){4, 3}, PIECE_PURPLE);
	setPiece(board, (Position){4, 4}, PIECE_GREEN);
}