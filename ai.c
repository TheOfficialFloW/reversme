/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "game.h"
#include "utils.h"
#include "ai.h"

typedef struct {
	Board board;
	Position pos;
} MovesList;

int getMoves(Board *board, MovesList *list, int player) {
	int count = 0;

	int x;
	for (x = 0; x < BOARD_RANGE; x++) {
		int y;
		for (y = 0; y < BOARD_RANGE; y++) {
			Position pos = (Position){x, y};
			if (getPiecesToReverse(board, pos, player, 1)) {
				list[count].pos = pos;
				memcpy(&list[count].board, board, sizeof(Board));
				reversePieces(&list[count].board, list[count].pos, player);
				count++;
			}
		}
	}

	return count;
}

double evaluation(Board *board, int player) {
	double result = 0;

	if (isGameOver(board)) {
		Position score = getScore(board);

		if (score.x == score.y) {
			result = 0;
		} else if (player == PIECE_PURPLE && score.x > score.y) {
			result = INF;
		} else if (player == PIECE_GREEN && score.x < score.y) {
			result = INF;
		} else {
			result = -INF;
		}
	} else {
		int weight[4][4] = {
			{ 120, -20, 20,  5 },
			{ -20, -40, -5, -5 },
			{  20,  -5, 15,  3 },
			{   5,  -5,  3,  3 }
		};

		int x;
		for (x = 0; x < BOARD_RANGE; x++) {
			int y;
			for (y = 0; y < BOARD_RANGE; y++) {
				int piece = getPiece(board, (Position){x, y});

				int u = (x / 4) ? (7 - x) : x;
				int v = (y / 4) ? (7 - y) : y;
				int w = weight[u][v];

				if (piece == player) {
					result += w;
				} else if (piece == getOpponent(player)) {
					result -= w;
				}
			}
		}
	}

	return result;
}

double minimax(Board *board, double alpha, double beta, int depth, int player, int min, Position *pos) {
	if (isGameOver(board) || depth == 0) {
		return evaluation(board, player);
	}

	MovesList *list = malloc(BOARD_LEN * sizeof(MovesList));
	int count = getMoves(board, list, min ? getOpponent(player) : player);

	Position best_pos = INVALID_POSITION;
	double best_value = min ? INF : -INF;

	int i;
	for (i = 0; i < count; i++) {
		double value = minimax(&list[i].board, alpha, beta, depth - 1, player, !min, NULL);

		if (!isOnBoard(best_pos) || (min && value < best_value) || (!min && value > best_value)) {
			best_pos = list[i].pos;
			best_value = value;
		}

		// Alpha-Beta pruning
		if (min) {
			if (alpha >= best_value) {
				break;
			}

			beta = MIN(beta, best_value);
		} else {
			if (beta < best_value) {
				break;
			}

			alpha = MAX(alpha, best_value);
		}
	}

	free(list);

	if (pos)
		*pos = best_pos;

	return best_value;
}

Position getBestMove(Board *board, int player, int depth) {
	Position pos;
	minimax(board, -INF, INF, depth, player, 0, &pos);
	return pos;
}