/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "menu.h"
#include "game.h"
#include "ai.h"
#include "config.h"
#include "utils.h"

#include "eurocaps.h"
#include "splashscreen.h"

#include "language.h"

PSP2_MODULE_INFO(0, 0, "ReversMe");

vita2d_font *font = NULL;

Game game;
ReversMeConfig reversme_config;
LanguageContainer language_container;

void deleteAIThread() {
	if (game.ai_thid >= 0) {
		sceKernelDeleteThread(game.ai_thid);
		sceKernelWaitThreadEnd(game.ai_thid, NULL, NULL);
		game.ai_thid = -1;
	}
}

void initGame(int player, int cpu) {
	deleteAIThread();

	Board board;
	memcpy(&board, &game.board, sizeof(Board));

	memset(&game, 0, sizeof(Game));

	initBoard(&game.board);
	memcpy(&game.old_board, &board, sizeof(Board));

	game.running = 1;
	game.game_over = 0;

	game.current_player = PIECE_PURPLE; // Purple goes first
	game.player = player;
	game.cpu = cpu;

	strcpy(game.message, "");
	game.message_counter = -1;
	game.window_height = 0;

	game.pieces = NULL;
	game.piece_pos = INVALID_POSITION;

	game.cursor_pos = (Position){4, 4};
	game.cursor_tick = 0;

	game.ai_thid = -1;
	game.ai_pos = INVALID_POSITION;
	game.ai_cursor_pos = INVALID_POSITION;

	game.alpha = NOALPHA;
	game.alpha_interval = 5;

	game.reinit_board = 0;
	game.reinit_animation = -1;

	game.animation = -1;
}

int AIThread(SceSize args, void *argp) {
	randomWait(500 * 1000, 1000 * 1000); // 0.5-1s

	Position pos = getBestMove(&game.board, game.current_player, reversme_config.ai_level);
	game.ai_cursor_pos = pos;

	randomWait(1000 * 1000, 2000 * 1000); // 1-2s

	game.ai_pos = pos;

	return sceKernelExitDeleteThread(0);
}

uint32_t getPieceColor(int piece) {
	uint32_t color;

	switch (piece) {
		case PIECE_EMPTY:
			color = GREY;
			break;
			
		case PIECE_PURPLE:
			color = PURPLE;
			break;
			
		case PIECE_GREEN:
			color = GREEN;
			break;
	}

	return color;
}

void drawPiece(Position pos, int piece, int alpha) {
	drawRectangle(getPositionByPiece(pos), PIECE_SIZE, PIECE_SIZE, (getPieceColor(piece) & 0x00FFFFFF) | ((alpha & 0xFF) << 24));
}

void drawBlinkPiece(Position pos) {
	if (isOnBoard(pos)) {
		if (getPiece(&game.board, pos) == PIECE_EMPTY)
			drawPiece(pos, game.current_player, game.alpha);

		drawBorder(getPositionByPiece(pos), PIECE_SIZE, PIECE_SIZE, 6, BLACK);
	}

	if (game.alpha >= NOALPHA || game.alpha <= 15)
		game.alpha_interval = -game.alpha_interval;

	game.alpha += game.alpha_interval;
}

void drawDetails() {
	// Draw title
	vita2d_font_draw_text(font, MENU_X, 2, PURPLE, FONT_BIGGEST, "revers");
	vita2d_font_draw_text(font, MENU_X + vita2d_font_text_width(font, FONT_BIGGEST, "revers"), 2, GREEN, FONT_BIGGEST, "me");

	// Draw score
	Position score = getScore(&game.board);

	char score_purple[4], score_green[4];
	sprintf(score_purple, "%d", score.x);
	sprintf(score_green, "%d", score.y);

	drawPiece((Position){-1, 0}, PIECE_PURPLE, NOALPHA);
	drawPiece((Position){8, 0}, PIECE_GREEN, NOALPHA);

	Position pos_center;

	pos_center = alignCenter((Position){BOARD_X - PIECE_SIZE - PIECE_BORDER, 0}, (Position){BOARD_X - PIECE_BORDER, 0}, vita2d_font_text_width(font, FONT_BIG, score_purple), 0);
	vita2d_font_draw_text(font, pos_center.x, 8, WHITE, FONT_BIG, score_purple);

	pos_center = alignCenter((Position){BOARD_X + BOARD_RANGE * PIECE_TOTAL_SIZE, 0}, (Position){BOARD_X + BOARD_RANGE * PIECE_TOTAL_SIZE + PIECE_SIZE, 0}, vita2d_font_text_width(font, FONT_BIG, score_green), 0);
	vita2d_font_draw_text(font, pos_center.x, 8, WHITE, FONT_BIG, score_green);

	if (game.running) {
		// Show player's turn
		if (!game.game_over)
			drawBorder(game.current_player == PIECE_PURPLE ? getPositionByPiece((Position){-1, 0}) : getPositionByPiece((Position){8, 0}), PIECE_SIZE, PIECE_SIZE, 6, BLACK);

		// Show title
		vita2d_font_draw_text(font, MENU_X, 476, BLACK, FONT_MIDDLE, game.cpu ? language_container.singleplayer : language_container.multiplayer);
	}
}

void drawBoard() {
	int x;
	for (x = 0; x < BOARD_RANGE; x++) {
		int y;
		for (y = 0; y < BOARD_RANGE; y++) {
			Position pos = (Position){x, y};

			// Draw piece
			drawPiece(pos, getPiece(&game.board, pos), NOALPHA);

			// Draw valid moves
			if (reversme_config.show_valid_moves && game.running) {
				if (getPiecesToReverse(&game.board, pos, game.current_player, 1)) {
					Position piece_pos = getPositionByPiece(pos);
					drawRectangle(alignCenter(piece_pos, (Position){piece_pos.x + PIECE_SIZE, piece_pos.y + PIECE_SIZE}, HINT_SIZE, HINT_SIZE), HINT_SIZE, HINT_SIZE, DARKGREY);
				}
			}
		}
	}
}

void drawReInitBoardAnimation() {
	if (game.reinit_board) {
		int x;
		for (x = 0; x < BOARD_RANGE; x++) {
			int y;
			for (y = 0; y < BOARD_RANGE; y++) {
				Position pos = (Position){x, y};

				int is_start_piece = 0;
				if ((x == 3 && y == 3) || (x == 3 && y == 4) || (x == 4 && y == 3) || (x == 4 && y == 4))
					is_start_piece = 1;

				if (game.reinit_board == 1) {
					if (is_start_piece) {
						drawPiece(pos, getPiece(&game.old_board, pos), NOALPHA);
					} else {
						Position piece_pos = getPositionByPiece(pos);
						drawRectangle(alignCenter(piece_pos, (Position){piece_pos.x + PIECE_SIZE, piece_pos.y + PIECE_SIZE}, game.reinit_animation, game.reinit_animation), game.reinit_animation, game.reinit_animation, getPieceColor(getPiece(&game.old_board, pos)));
					}
				} else if (game.reinit_board == 2) {
					if (is_start_piece) {
						drawPiece(pos, getPiece(&game.old_board, pos), NOALPHA);
						Position piece_pos = getPositionByPiece(pos);
						drawRectangle(alignCenter(piece_pos, (Position){piece_pos.x + PIECE_SIZE, piece_pos.y + PIECE_SIZE}, game.reinit_animation, game.reinit_animation), game.reinit_animation, game.reinit_animation, getPieceColor(getPiece(&game.board, pos)));
					}
				}
			}
		}

		if (game.reinit_board == 1) {
			if (game.reinit_animation > 0) {
				game.reinit_animation -= ANIMATION_INTERVAL;
			} else {
				game.reinit_board = 2;
			}
		} else if (game.reinit_board == 2) {
			if (game.reinit_animation < PIECE_SIZE) {
				game.reinit_animation += ANIMATION_INTERVAL;
			} else {
				game.reinit_board = 0;
				game.reinit_animation = -1;
				game.running = 1;
			}
		}
	}
}

void drawAnimation() {
	if (game.pieces && game.animation != -1) {
		int i;
		for (i = 0; i < BOARD_LEN; i++) {
			Position pos = game.pieces[i];

			if (!isOnBoard(pos))
				break;

			Position piece_pos = getPositionByPiece(pos);
			drawRectangle(alignCenter(piece_pos, (Position){piece_pos.x + PIECE_SIZE, piece_pos.y + PIECE_SIZE}, game.animation, game.animation), game.animation, game.animation, getPieceColor(getOpponent(getPiece(&game.board, pos))));
		}

		if (game.animation < PIECE_SIZE)
			game.animation += ANIMATION_INTERVAL;
	}
}

void drawBlinkingPiece() {
	if (!isMenuOpen() && !isMenuScrolling()) {
		uint64_t tick;
		sceRtcGetCurrentTick(&tick);

		// Cursor disappear after 5s
		if (game.cursor_tick && (tick - game.cursor_tick) >= 5 * 1000 * 1000)
			game.cursor_tick = 0;

		if (game.current_player != game.cpu) {
			if (game.cursor_tick)
				drawBlinkPiece(game.cursor_pos);
		} else {
			drawBlinkPiece(game.ai_cursor_pos);
		}
	}
}

void drawMessage() {
	if (game.message_counter >= 0) {
		// Opening window
		drawRectangle(alignCenter((Position){BOARD_X, BOARD_Y}, (Position){BOARD_X, BOARD_Y + BOARD_SIZE}, 0, game.window_height), BOARD_SIZE, game.window_height, RGBA8(0, 0, 0, 200));

		if (game.window_height < WINDOW_HEIGHT) {
			game.window_height += WINDOW_INTERVAL;
		} else {
			Position pos = alignCenter((Position){BOARD_X, BOARD_Y}, (Position){BOARD_X + BOARD_SIZE, BOARD_Y}, vita2d_font_text_width(font, FONT_BIG, game.message), 0);
			vita2d_font_draw_text(font, pos.x, 238, WHITE, FONT_BIG, game.message);
		}

		game.message_counter++;

		// Abort after a while
		if (game.message_counter >= 80) {
			game.message_counter = -1;
			game.window_height = 0;
		}
	}	
}

void drawGame() {
	// Draw details
	drawDetails();

	// Draw board
	drawBoard();

	if (game.running) {
		// Draw new set piece
		if (isOnBoard(game.piece_pos))
			drawPiece(game.piece_pos, game.current_player, NOALPHA);

		// Draw animation
		drawAnimation();

		// Draw blinking piece
		drawBlinkingPiece();

		// Draw message
		drawMessage();
	} else {
		// Draw reinit board animation
		drawReInitBoardAnimation();
	}
}

void gameCtrl() {
	if (game.current_player != game.cpu) {
		int pressed = 0;

		if (game.cpu || game.current_player == PIECE_PURPLE) {
			if (hold_buttons & PSP2_CTRL_LEFT || hold_buttons & PSP2_CTRL_LEFT_ANALOG_LEFT) {
				pressed |= PSP2_CTRL_LEFT;
			} else if (hold_buttons & PSP2_CTRL_RIGHT || hold_buttons & PSP2_CTRL_LEFT_ANALOG_RIGHT) {
				pressed |= PSP2_CTRL_RIGHT;
			}

			if (hold_buttons & PSP2_CTRL_UP || hold_buttons & PSP2_CTRL_LEFT_ANALOG_UP) {
				pressed |= PSP2_CTRL_UP;
			} else if (hold_buttons & PSP2_CTRL_DOWN || hold_buttons & PSP2_CTRL_LEFT_ANALOG_DOWN) {
				pressed |= PSP2_CTRL_DOWN;
			}
		} else if (game.current_player == PIECE_GREEN) {
			if (hold_buttons & PSP2_CTRL_SQUARE || hold_buttons & PSP2_CTRL_RIGHT_ANALOG_LEFT) {
				pressed |= PSP2_CTRL_LEFT;
			} else if (hold_buttons & PSP2_CTRL_CIRCLE || hold_buttons & PSP2_CTRL_RIGHT_ANALOG_RIGHT) {
				pressed |= PSP2_CTRL_RIGHT;
			}

			if (hold_buttons & PSP2_CTRL_TRIANGLE || hold_buttons & PSP2_CTRL_RIGHT_ANALOG_UP) {
				pressed |= PSP2_CTRL_UP;
			} else if (hold_buttons & PSP2_CTRL_CROSS || hold_buttons & PSP2_CTRL_RIGHT_ANALOG_DOWN) {
				pressed |= PSP2_CTRL_DOWN;
			}
		}
		
		if (pressed & PSP2_CTRL_LEFT) {
			if (game.cursor_pos.x > 0)
				game.cursor_pos.x--;
		} else if (pressed & PSP2_CTRL_RIGHT) {
			if (game.cursor_pos.x < BOARD_RANGE-1)
				game.cursor_pos.x++;
		}

		if (pressed & PSP2_CTRL_UP) {
			if (game.cursor_pos.y > 0)
				game.cursor_pos.y--;
		} else if (pressed & PSP2_CTRL_DOWN) {
			if (game.cursor_pos.y < BOARD_RANGE-1)
				game.cursor_pos.y++;
		}

		if (pressed)
			sceRtcGetCurrentTick(&game.cursor_tick);
	}

	// Play if no animation and no message
	if((game.animation == -1 || game.animation == PIECE_SIZE) && game.message_counter == -1) {
		if (isOnBoard(game.piece_pos)) { // Played
			// Stop animation
			game.animation = -1;

			// Reverse pieces
			reversePieces(&game.board, game.piece_pos, game.current_player);
			game.piece_pos = INVALID_POSITION;
			game.pieces = NULL;

			// Change current player
			game.current_player = getOpponent(game.current_player);

			if (isGameOver(&game.board)) { // Game over
				game.game_over = 1;
				openMenu(GAME_OVER_MENU);
			} else if (noValidMoves(&game.board, game.current_player)) { // Pass
				strcpy(game.message, language_container.pass);
				game.message_counter = 0;
				game.current_player = getOpponent(game.current_player);
			}
		} else {
			if (game.current_player != game.cpu) { // Human
				Position pos;

				if (released_touch.x != -1 && released_touch.y != -1) {
					pos = getPieceByPosition(released_touch);
					game.cursor_tick = 0; // Remove cursor if touched
				}

				if (game.cursor_tick) {
					if (game.cpu) {
						if (released_buttons & PSP2_CTRL_CROSS) {
							pos = game.cursor_pos;
						}
					} else {
						if ((game.current_player == PIECE_PURPLE && released_buttons & PSP2_CTRL_LTRIGGER) || (game.current_player == PIECE_GREEN && released_buttons & PSP2_CTRL_RTRIGGER)) {
							pos = game.cursor_pos;
						}
					}
				}

				if (isOnBoard(pos)) {
					game.pieces = getPiecesToReverse(&game.board, pos, game.current_player, 0);
					if (game.pieces) {
						game.piece_pos = pos;
						game.animation = 0; // Start animation
					} else {
						strcpy(game.message, language_container.invalid);
						game.message_counter = 0;
					}
				}
			} else { // AI
				if (isOnBoard(game.ai_pos)) {
					game.pieces = getPiecesToReverse(&game.board, game.ai_pos, game.current_player, 0);
					if (game.pieces) {
						game.piece_pos = game.ai_pos;
						game.animation = 0; // Start animation

						deleteAIThread();

						game.ai_pos = INVALID_POSITION;
						game.ai_cursor_pos = INVALID_POSITION;
					}
				} else {
					if (game.ai_thid < 0) {
						game.ai_thid = sceKernelCreateThread("AIThread", (SceKernelThreadEntry)AIThread, 0x10000100, 0x10000, 0, 0, NULL);
						if (game.ai_thid >= 0)
							sceKernelStartThread(game.ai_thid, 0, NULL);
					}
				}
			}
		}
	}
}

void gameMain() {
	memset(&game, 0, sizeof(Game));

	initGame(PIECE_PURPLE, 0);

	game.running = 0;

	openMenu(MAIN_MENU);

	while (1) {
		// Read controls and touchscreen
		ReadPad();

		// Start drawing, clear screen white
		vita2d_start_drawing();
		vita2d_set_clear_color(WHITE);
		vita2d_clear_screen();

		// Draw game
		drawGame();

		// Draw menu
		drawMenu();

		// Menu is not scrolling
		if(!isMenuScrolling()) {
			// Menu or game control
			if (isMenuOpen()) {
				menuCtrl();
			} else {
				gameCtrl();
			}

			// Pause menu
			if (game.running && !game.game_over && !isMenuCalling()) {
				if (isMenuOpen()) {
					if (released_buttons & PSP2_CTRL_START || (released_touch.x >= BOARD_X && released_touch.x < (BOARD_X + BOARD_SIZE) && released_touch.y >= BOARD_Y && released_touch.y < (BOARD_Y + BOARD_SIZE)))
						closeMenu();
				} else {
					if (released_buttons & PSP2_CTRL_START || (released_touch.x >= 0 && released_touch.x < BOARD_X && released_touch.y >= (BOARD_Y + PIECE_TOTAL_SIZE) && released_touch.y < (BOARD_Y + 7 * PIECE_TOTAL_SIZE)))
						openMenu(PAUSE_MENU);
				}
			}
		}

		// End draw
		vita2d_end_drawing();
		vita2d_swap_buffers();
	}
}

void showSplashScreen() {
	vita2d_texture *splash = vita2d_create_empty_texture(SCREEN_WIDTH, SCREEN_HEIGHT);
	splash = vita2d_load_PNG_buffer(splashscreen);

	int fade_mode = 0, fade_alpha = NOALPHA;

	while (fade_mode < 2) {
		vita2d_start_drawing();
		vita2d_clear_screen();

		vita2d_draw_texture(splash, 0, 0);
		vita2d_draw_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, RGBA8(0, 0, 0, fade_alpha));

		vita2d_end_drawing();
		vita2d_swap_buffers();

		if (!fade_mode) {
			if (fade_alpha > 0) {
				fade_alpha -= 3;
			} else {
				fade_mode++;
				sceKernelDelayThread(2 * 1000 * 1000); // 2s
			}
		} else {
			if (fade_alpha < NOALPHA) {
				fade_alpha += 3;
			} else {
				fade_mode++;
			}
		}
	}

	// vita2d_free_texture(splash); // chrashes
}

void initAppUtil() {
	SceAppUtilInitParam	init_param;
	SceAppUtilBootParam	boot_param;
	memset(&init_param, 0 ,sizeof(SceAppUtilInitParam));
	memset(&boot_param, 0, sizeof(SceAppUtilBootParam));
	sceAppUtilInit(&init_param, &boot_param);	
}

void initLanguage() {
	int language;
	sceAppUtilSystemParamGetInt(PSP2_SYSTEM_PARAM_ID_LANG, &language);

	LanguageContainer *container;

	switch (language) {
		case PSP2_SYSTEM_PARAM_LANG_FRENCH:
			container = &french_container;
			break;

		case PSP2_SYSTEM_PARAM_LANG_SPANISH:
			container = &spanish_container;
			break;

		case PSP2_SYSTEM_PARAM_LANG_GERMAN:
			container = &german_container;
			break;

		case PSP2_SYSTEM_PARAM_LANG_DUTCH:
			container = &dutch_container;
			break;

		case PSP2_SYSTEM_PARAM_LANG_POLISH:
			container = &polish_container;
			break;

		default:
			container = &english_container;
			break;
	}

	memcpy(&language_container, container, sizeof(LanguageContainer));
}

int main() {
	sceCtrlSetSamplingMode(PSP2_CTRL_MODE_ANALOG);

	srand(time(NULL));

	// Init AppUtil
	initAppUtil();

	// Init language
	initLanguage();

	// Init graphics library
	vita2d_init();
	vita2d_set_vblank_wait(1);

	// Load font
	font = vita2d_load_font_mem(eurocaps, sizeof(eurocaps));

	// Show splash screen
    showSplashScreen();

	// Load config
	loadConfig(&reversme_config);

	// Game main
	gameMain();

	// Free
	vita2d_free_font(font);
	vita2d_fini();

	// Exit
	sceKernelExitProcess(0);
	return 0;
}