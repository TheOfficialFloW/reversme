/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MAIN_H__
#define __MAIN_H__

#include <psp2/apputil.h>
#include <psp2/ctrl.h>
#include <psp2/kernel/processmgr.h>
#include <psp2/io/fcntl.h>
#include <psp2/io/dirent.h>
#include <psp2/types.h>
#include <psp2/moduleinfo.h>
#include <psp2/rtc.h>
#include <psp2/system_param.h>
#include <psp2/touch.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <malloc.h>

#include <math.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <vita2d.h>

#include "game.h"
#include "config.h"

// Color
#define BLACK	 RGBA8(  0,   0,   0, 255)
#define DARKGREY RGBA8(127, 127, 127, 255)
#define GREY	 RGBA8(195, 195, 195, 255)
#define WHITE	 RGBA8(255, 255, 255, 255)
#define PURPLE   RGBA8(163,  73, 164, 255)
#define GREEN	 RGBA8( 34, 177,  76, 255)
#define RED	     RGBA8(255,   0,   0, 255)

#define NOALPHA 255

// Screen
#define SCREEN_WIDTH 960
#define SCREEN_HEIGHT 544

// Board
#define BOARD_X 360
#define BOARD_Y 10

// Menu
#define MENU_X 10
#define MENU_Y 80

// Pieces
#define HINT_SIZE 8
#define PIECE_SIZE 62
#define PIECE_BORDER 4
#define PIECE_TOTAL_SIZE (PIECE_SIZE + PIECE_BORDER)

// Animation
#define ANIMATION_INTERVAL 2

// Window
#define WINDOW_HEIGHT 60
#define WINDOW_INTERVAL 6

// AI
#define AI_LEVEL_MIN 1
#define AI_LEVEL_DEFAULT 3
#define AI_LEVEL_MAX 6

// Font
#define FONT_SMALLEST 16
#define FONT_SMALL 26
#define FONT_MIDDLE 36
#define FONT_BIG 46
#define FONT_BIGGEST 56

/*
	Biggest
	-8
	
	Big
	+2

	Middle
	+4

	Small
	+18
*/

typedef struct {
	char main_menu[32];
	char singleplayer[32];
	char multiplayer[32];

	char start_game[32];
	char restart_game[32];
	char exit_game[32];

	char your_color[32];
	char ai_level[32];
	char show_valid_moves[32];

	char pause[32];

	char pass[32];
	char invalid[32];
	char purple_won[32];
	char green_won[32];
	char you_won[32];
	char you_lost[32];
	char drawn[32];

	char purple[32];
	char green[32];

	char yes[32];
	char no[32];

	char back[32];
} LanguageContainer;

typedef struct {
	Board board;
	Board old_board;

	// State
	int running;
	int game_over;

	// Player
	int current_player;
	int player;
	int cpu;

	Position *pieces;
	Position piece_pos;

	// Message
	char message[16];
	int message_counter;
	int window_height;

	// Cursor
	Position cursor_pos;
	uint64_t cursor_tick;

	// AI
	SceUID ai_thid;
	Position ai_pos;
	Position ai_cursor_pos;

	// Blinking
	int alpha;
	int alpha_interval;

	// Reinit
	int reinit_board;
	int reinit_animation;

	// Animation
	int animation;
} Game;

extern Game game;
extern ReversMeConfig reversme_config;
extern LanguageContainer language_container;

extern vita2d_font *font;

void deleteAIThread();
void initGame(int player, int cpu);

#endif