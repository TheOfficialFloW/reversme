TARGET = reversme
OBJS   = main.o menu.o game.o ai.o config.o utils.o

LIBS = -lSceAppUtil_stub -lSceCtrl_stub -lSceDisplay_stub -lSceGxm_stub -lSceTouch_stub -lSceRtc_stub -lc_stub -lm_stub -lpng -lz -lfreetype -lvita2d

PREFIX  = $(DEVKITARM)/bin/arm-none-eabi
CC      = $(PREFIX)-gcc
CFLAGS  = -Wall -specs=psp2.specs
ASFLAGS = $(CFLAGS)

all: $(TARGET)_fixup.elf
	make copy

%_fixup.elf: %.elf
	psp2-fixup -q -S $< $@

$(TARGET).elf: $(OBJS)
	$(CC) $(CFLAGS) $^ $(LIBS) -o $@

clean:
	@rm -rf $(TARGET)_fixup.elf $(TARGET).elf $(OBJS)

copy: $(TARGET)_fixup.elf
	@cp $(TARGET)_fixup.elf ../Rejuvenate/$(TARGET)_fixup.elf
	@echo "Copied!"
