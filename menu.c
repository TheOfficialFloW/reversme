/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "menu.h"
#include "config.h"

static char game_over_text[32], your_color_text[32], ai_level_text[32], show_valid_moves_text[32];

static int menu_mode = 0;
static int menu_sel = 0;
static int menu_flags = 0;

static int menu_scroll_y = SCREEN_HEIGHT;

void openMenu(int mode) {
	menu_mode = mode;
	menu_sel = 0;
	menu_flags |= MENU_FLAG_OPEN;
}

void closeMenu() {
	menu_flags &= ~MENU_FLAG_OPEN;
}

int isMenuOpen() {
	return menu_flags & MENU_FLAG_OPEN;
}

int getMenuMode() {
	return menu_mode;
}

int isMenuScrolling() {
	return menu_flags & MENU_FLAG_SCROLLING;
}

int isMenuCalling() {
	return menu_flags & MENU_FLAG_CALLING;
}

void backToGame() {
	// Do nothing
}

void yourColor() {
	reversme_config.player = getOpponent(reversme_config.player);
}

void aiLevel() {
	if (reversme_config.ai_level < AI_LEVEL_MAX) {
		reversme_config.ai_level++;
	} else {
		reversme_config.ai_level = AI_LEVEL_MIN;
	}
}

void showValidMoves() {
	reversme_config.show_valid_moves = !reversme_config.show_valid_moves;
}

void backToMainMenu() {
	saveConfig(&reversme_config);
	openMenu(MAIN_MENU);
}

void exitGame() {
	deleteAIThread();
	game.running = 0;
	openMenu(MAIN_MENU);
}

void reInitBoard(int player, int cpu) {
	initGame(player, cpu);

	game.running = 0;
	game.reinit_board = 1;
	game.reinit_animation = PIECE_SIZE;
}

void restartGame() {
	reInitBoard(game.player, game.cpu);
}

void startSinglePlayerGame() {
	saveConfig(&reversme_config);
	reInitBoard(reversme_config.player, getOpponent(reversme_config.player));
}

void startMultiPlayerGame() {
	saveConfig(&reversme_config);
	reInitBoard(PIECE_PURPLE, 0);
}

void singlePlayer() {
	openMenu(SINGLE_PLAYER_MENU);
}

void multiPlayer() {
	openMenu(MULTI_PLAYER_MENU);
}

static Entry main_menu_entries[] = {
	{ language_container.singleplayer, singlePlayer, 1 },
	{ language_container.multiplayer, multiPlayer, 1 },
};

static Entry single_player_entries[] = {
	{ language_container.start_game, startSinglePlayerGame, 1 },
	{ your_color_text, yourColor, 0 },
	{ ai_level_text, aiLevel, 0 },
	{ show_valid_moves_text, showValidMoves, 0 },
	{ language_container.back, backToMainMenu, 1 },
};

static Entry multi_player_entries[] = {
	{ language_container.start_game, startMultiPlayerGame, 1 },
	{ show_valid_moves_text, showValidMoves, 0 },
	{ language_container.back, backToMainMenu, 1 },
};

static Entry pause_menu_entries[] = {
	{ language_container.back, backToGame, 1 },
	{ language_container.restart_game, restartGame, 1 },
	{ language_container.exit_game, exitGame, 1 },
};

static Entry game_over_menu_entries[] = {
	{ language_container.restart_game, restartGame, 1 },
	{ language_container.back, exitGame, 1 },
};

static Menu menus[] = {
	{ language_container.main_menu, main_menu_entries, sizeof(main_menu_entries) / sizeof(Entry), NULL },
	{ language_container.singleplayer, single_player_entries, sizeof(single_player_entries) / sizeof(Entry), backToMainMenu },
	{ language_container.multiplayer, multi_player_entries, sizeof(multi_player_entries) / sizeof(Entry), backToMainMenu },
	{ language_container.pause, pause_menu_entries, sizeof(pause_menu_entries) / sizeof(Entry), backToGame },
	{ game_over_text, game_over_menu_entries, sizeof(game_over_menu_entries) / sizeof(Entry), exitGame },
};

void drawMenu() {
	// Menu scroll
	if (isMenuOpen()) {
		if (menu_scroll_y > (MENU_Y + 79)) {
			menu_scroll_y -= SCROLL_INTERVAL;
			menu_flags |= MENU_FLAG_SCROLLING;
		} else {
			menu_flags &= ~MENU_FLAG_SCROLLING;
		}
	} else {
		if (menu_scroll_y < SCREEN_HEIGHT) {
			menu_scroll_y += SCROLL_INTERVAL;
			menu_flags |= MENU_FLAG_SCROLLING;
		} else {
			menu_flags &= ~MENU_FLAG_SCROLLING;
		}
	}

	// Call when it's not scrolling anymore
	if (isMenuCalling() && !isMenuScrolling()) {
		if (menu_flags & MENU_FLAG_BACK_FUNCTION) {
			menus[menu_mode].back_function();
			menu_flags &= ~MENU_FLAG_BACK_FUNCTION;
		} else {
			if (menus[menu_mode].entries[menu_sel].function)
				menus[menu_mode].entries[menu_sel].function();
		}

		menu_flags &= ~MENU_FLAG_CALLING;
	}

	// Draw
	if (isMenuOpen() || isMenuScrolling()) {
		if (menu_mode == SINGLE_PLAYER_MENU) {
			sprintf(your_color_text, "%s: %s", language_container.your_color, reversme_config.player == PIECE_PURPLE ? language_container.purple : language_container.green);
			sprintf(ai_level_text, "%s: %d", language_container.ai_level, reversme_config.ai_level);			
		} else if (menu_mode == GAME_OVER_MENU) {
			Position score = getScore(&game.board);

			if (game.cpu) {
				if ((game.player == PIECE_PURPLE && score.x > score.y) || (game.player == PIECE_GREEN && score.x < score.y)) {
					strcpy(game_over_text, language_container.you_won);
				} else if ((game.player == PIECE_PURPLE && score.x < score.y) || (game.player == PIECE_GREEN && score.x > score.y)) {
					strcpy(game_over_text, language_container.you_lost);
				}
			} else {
				if (score.x > score.y) {
					strcpy(game_over_text, language_container.purple_won);
				} else if (score.x < score.y) {
					strcpy(game_over_text, language_container.green_won);
				}
			}

			if (score.x == score.y)
				strcpy(game_over_text, language_container.drawn);
		}

		if (menu_mode == SINGLE_PLAYER_MENU || menu_mode == MULTI_PLAYER_MENU)
			sprintf(show_valid_moves_text, "%s: %s", language_container.show_valid_moves, reversme_config.show_valid_moves ? language_container.yes : language_container.no);

		// Title
		if (isMenuOpen())
			vita2d_font_draw_text(font, MENU_X, MENU_Y, BLACK, FONT_MIDDLE, menus[menu_mode].title);

		// Entries
		int i;
		for (i = 0; i < menus[menu_mode].n_entries; i++) {
			int y = menu_scroll_y + i * PIECE_TOTAL_SIZE;
			vita2d_font_draw_text(font, MENU_X, y, BLACK, FONT_SMALL, menus[menu_mode].entries[i].text);

			// drawBorder((Position){0, y - CORRECTION}, BOARD_X, PIECE_TOTAL_SIZE, 1, RED);

			// Underline
			if (i == menu_sel)
				vita2d_draw_rectangle(MENU_X, y + 30, vita2d_font_text_width(font, FONT_SMALL, menus[menu_mode].entries[i].text), 2, BLACK);
		}
	}
}

void menuCtrl() {
	int selected = 0;

	if (hold_buttons & PSP2_CTRL_UP || hold_buttons & PSP2_CTRL_LEFT_ANALOG_UP) {
		if (menu_sel > 0) {
			menu_sel--;
		} else {
			menu_sel = menus[menu_mode].n_entries - 1;
		}
	} else if (hold_buttons & PSP2_CTRL_DOWN || hold_buttons & PSP2_CTRL_LEFT_ANALOG_DOWN) {
		if (menu_sel < menus[menu_mode].n_entries - 1) {
			menu_sel++;
		} else {
			menu_sel = 0;
		}
	}

	if (released_buttons & PSP2_CTRL_CIRCLE) {
		if (menus[menu_mode].back_function) {
			menu_flags |= MENU_FLAG_CALLING;
			menu_flags |= MENU_FLAG_BACK_FUNCTION;
			closeMenu();
		}
	} else if (released_buttons & PSP2_CTRL_CROSS) {
		selected = 1;
	} else {
		int i;
		for (i = 0; i < menus[menu_mode].n_entries; i++) {
			int y = menu_scroll_y + i * PIECE_TOTAL_SIZE - CORRECTION;
			if (released_touch.x >= 0 && released_touch.x < BOARD_X && released_touch.y >= y && released_touch.y < y + PIECE_TOTAL_SIZE) {
				menu_sel = i;
				selected = 1;
				break;
			}
		}
	}

	if (selected) {
		if (menus[menu_mode].entries[menu_sel].close_menu) {
			menu_flags |= MENU_FLAG_CALLING;
			closeMenu();
		} else {
			if (menus[menu_mode].entries[menu_sel].function)
				menus[menu_mode].entries[menu_sel].function();
		}
	}
}