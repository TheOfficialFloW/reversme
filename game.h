/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __GAME_H__
#define __GAME_H__

#include "main.h"
#include "utils.h"

// Must be unique values/strings
#define PIECE_PURPLE -1
#define PIECE_EMPTY   0
#define PIECE_GREEN   1

#define BOARD_RANGE 8
#define BOARD_LEN (BOARD_RANGE * BOARD_RANGE)
#define BOARD_SIZE (BOARD_RANGE * PIECE_TOTAL_SIZE - PIECE_BORDER)

typedef struct {
	int board[BOARD_RANGE][BOARD_RANGE];
} Board;

int getPiece(Board *board, Position pos);
void setPiece(Board *board, Position pos, int piece);
Position getPositionByPiece(Position pos);
Position getPieceByPosition(Position pos);
int isOnBoard(Position pos);
int getOpponent(int piece);
Position *getPiecesToReverse(Board *board, Position pos, int player, int check_valid_piece);
void reversePieces(Board *board, Position pos, int player);
Position getScore(Board *board);
int noValidMoves(Board *board, int player);
int numValidMoves(Board *board, int player);
int isGameOver(Board *board);
void initBoard(Board *board);
	
#endif