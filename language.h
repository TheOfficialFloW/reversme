/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"

LanguageContainer english_container = {
	"main menu",
	"single player",
	"multiplayer",
	"start game",
	"restart game",
	"exit game",
	"your color",
	"ai level",
	"show valid moves",
	"pause",
	"pass",
	"invalid",
	"purple won",
	"green won",
	"you won",
	"you lost",
	"drawn",
	"purple",
	"green",
	"yes",
	"no",
	"back",
};

LanguageContainer french_container = {
	"menu principal",
	"monojoueur",
	"multijoueur",
	"commencer la partie",
	"recommencer la partie",
	"terminer la partie",
	"ta couleur",
	"ia niveau",
	"montrer coups valides",
	"pause",
	"passer",
	"invalide",
	"pourpre a gagn\xE9",
	"vert a gagn\xE9",
	"gagn\xE9",
	"perdu",
	"partie nul",
	"pourpre",
	"vert",
	"oui",
	"non",
	"retour",
};

LanguageContainer spanish_container = {
	"men\xFA principal",
	"un jugador",
	"multijugador",
	"iniciar juego",
	"reiniciar juego",
	"salir del juego",
	"tu color",
	"dificultad de ia",
	"mostrar movimientos v\xE1lidos",
	"pausa",
	"pasar",
	"inv\xE1lido",
	"gana el morado",
	"gana el verde",
	"ganaste",
	"perdiste",
	"empate",
	"morado",
	"verde",
	"s\xED",
	"no",
	"atr\xE1s",
};

LanguageContainer german_container = {
	"hauptmen\xFC",
	"einzelspieler",
	"mehrspieler",
	"spiel starten",
	"spiel neu starten",
	"spiel beenden",
	"deine farbe",
	"ki stufe",
	"g\xFCltige z\xFCge anzeigen",
	"pause",
	"passen",
	"ung\xFCltig",
	"violett gewonnen",
	"gr\xFCn gewonnen",
	"gewonnen",
	"verloren",
	"unentschieden",
	"violett",
	"gr\xFCn",
	"ja",
	"nein",
	"zur\xFC\x63k",
};

LanguageContainer polish_container = {
	"menu glowne",
	"jednoosobowa",
	"wieloosobowa",
	"zaczni gre",
	"zrestartuj gre",
	"wyjdz z gry",
	"twoj kolor",
	"poziom trudnosci",
	"pokaz dobre ruchy",
	"pauza",
	"koniec",
	"nie poprawne",
	"fioletowy wygral",
	"zielony wygral",
	"wygrales",
	"przegrales",
	"remis",
	"foletowy",
	"zielony",
	"tak",
	"nie",
	"wroc",
};

LanguageContainer dutch_container = {
	"main menu",
	"een speler",
	"meer spelers",
	"start spel",
	"resetten spel",
	"exit spel",
	"jouw kleur",
	"ai moeilijkheid",
	"laat goede bewegingen zien",
	"pauze",
	"klaar",
	"verkeerd",
	"violet won",
	"groen won",
	"jij won",
	"jij verloren",
	"trek",
	"violet",
	"groen",
	"ja",
	"nee",
	"terug",
};