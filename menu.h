/*
	ReversMe
	Copyright (C) 2015, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MENU_H__
#define __MENU_H__

#include "game.h"

enum MenuModes
{
	MAIN_MENU = 0,
	SINGLE_PLAYER_MENU,
	MULTI_PLAYER_MENU,
	PAUSE_MENU,
	GAME_OVER_MENU,
};

enum MenuFlags
{
	MENU_FLAG_OPEN = 0x1,
	MENU_FLAG_SCROLLING = 0x2,
	MENU_FLAG_CALLING = 0x4,
	MENU_FLAG_BACK_FUNCTION = 0x8,
};

#define SCROLL_INTERVAL 12

#define CORRECTION 16

typedef struct {
	char *text;
	void (* function)();
	int close_menu;
} Entry;

typedef struct {
	char *title;
	Entry *entries;
	int n_entries;
	void (* back_function)();
} Menu;

void openMenu(int mode);
void closeMenu();
int isMenuOpen();
int getMenuMode();
int isMenuScrolling();
int isMenuCalling();
void drawMenu();
void menuCtrl();

#endif